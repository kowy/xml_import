package cz.kovar.david.xml_import.datasources;


import cz.kovar.david.xml_import.properties.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public enum DataSourceFactory {
    INSTANCE;
    private static final Logger log = LogManager.getLogger();

    DataSourceFactory() { }

    public IDataSource getDataSource(File file) {
        String extension = file.getName().substring(file.getName().lastIndexOf('.')+1).toUpperCase();
        log.debug("Creating DataSource for extension " + extension.toUpperCase());
        if (extension.equals("XML")) {
            return Configuration.INSTANCE.getContext().getBean("xmlDataSource", XmlDataSource.class);
        }

        return null;
    }
}
