package cz.kovar.david.xml_import.models.repositories;

import cz.kovar.david.xml_import.models.Factory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFactoryRepository extends CrudRepository<Factory, Integer> {
    List<Factory> findByName(String name);

    @Query("SELECT f FROM factory f where f.ico = ?1")
    Factory findByIco(String ico);
}
