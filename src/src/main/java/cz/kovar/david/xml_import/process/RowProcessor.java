package cz.kovar.david.xml_import.process;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class RowProcessor {
    private static final Logger log = LogManager.getLogger();

    /***
     * Column mapping (should be moved to some XML configuration)
     */
    private final HashMap<String, List<Map.Entry<String, String>>> xmlMapping = new HashMap<String, List<Map.Entry<String, String>>>() {{
        put("cz.kovar.david.xml_import.models.Employee",
                new ArrayList<Map.Entry<String, String>>() {{
                    add(new SimpleEntry<>("email", "setEmail"));
                    add(new SimpleEntry<>("fn", "setFirstName"));
                    add(new SimpleEntry<>("ln", "setLastName"));
                    add(new SimpleEntry<>("eaddr", "setAddress"));
                    add(new SimpleEntry<>("ico", "setFactoryByName"));
                }});
        put("cz.kovar.david.xml_import.models.Factory",
                new ArrayList<Map.Entry<String, String>>() {{
                    add(new SimpleEntry<>("ico", "setIco"));
                    add(new SimpleEntry<>("fname", "setName"));
                    add(new SimpleEntry<>("faddr", "setAddress"));
                }});
    }};

    /***
     * XML or CSV
     */
    private String type;

    public RowProcessor(String type) {
        this.type = type;
    }

    public <T> T processNewRow(HashMap<String, String> row, Class<T> destClass) {
        HashMap<String, List<Map.Entry<String, String>>> mapping;
        if (type.equals("XML")) mapping = xmlMapping;
        else mapping = xmlMapping;  // csvMapping

        List<Map.Entry<String, String>> attrMap = mapping.get(destClass.getCanonicalName());
        if (attrMap == null) {
            log.error("No mapping for class {} found", destClass.getCanonicalName());
            return null;
        }

        T instance;
        try {
            instance = destClass.newInstance();
        } catch (InstantiationException e) {
            log.error("Error creating instance of model {}: {}", destClass.getSimpleName(), e.getMessage());
            log.debug(e);
            return null;
        } catch (IllegalAccessException e) {
            log.error("Illegal access when creating instance of model {}: {}", destClass.getSimpleName(), e.getMessage());
            return null;
        }

        fillRowData(row, instance, destClass);

        return instance;
    }

    public <T> T fillRowData(HashMap<String, String> row, T instance, Class<T> destClass) {
        HashMap<String, List<Map.Entry<String, String>>> mapping;
        if (type.equals("XML")) mapping = xmlMapping;
        else mapping = xmlMapping;  // csvMapping

        List<Map.Entry<String, String>> attrMap = mapping.get(destClass.getCanonicalName());
        if (attrMap == null) {
            log.error("No mapping for class {} found", destClass.getCanonicalName());
            return null;
        }

        for (Map.Entry<String, String> mapPair : attrMap) {
            try {
                if (!row.containsKey(mapPair.getKey())) {
                    log.error("Row is missing value for attribute {} for conversion type {}", mapPair.getKey(), type);
                    continue;
                }

                Method method = destClass.getDeclaredMethod(mapPair.getValue(), String.class);
                method.invoke(instance, row.get(mapPair.getKey()));
            } catch (NoSuchMethodException e) {
                log.info("No method {} on model {}. Ignoring", mapPair.getValue(), destClass.getSimpleName());
            } catch (InvocationTargetException e) {
                log.info("Error calling method {} on model {}: {}", mapPair.getValue(), destClass.getSimpleName(), e.getMessage());
                log.debug(e);
            } catch (IllegalAccessException e) {
                log.info("Error access method {} on model {}: {}", mapPair.getValue(), destClass.getSimpleName(), e.getMessage());
                log.debug(e);
            }
        }

        return instance;
    }
}
