package cz.kovar.david.xml_import.datasources;

import java.io.InputStream;
import java.util.HashMap;

public interface IDataSource {
    void open(InputStream stream);
    HashMap<String, String> getNextRow();
    boolean hasNextRow();
    void close();
}
