package cz.kovar.david.xml_import.models.repositories;

import cz.kovar.david.xml_import.models.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmployeeRepository extends CrudRepository<Employee, Integer> {
    @Query("SELECT e FROM employee e where e.email = ?1")
    Employee findByEmail(String email);
}
