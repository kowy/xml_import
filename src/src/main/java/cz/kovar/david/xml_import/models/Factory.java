package cz.kovar.david.xml_import.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

@Entity(name = "factory")
public class Factory {
    private static final Logger log = LogManager.getLogger();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String ico;
    @Column(name = "fac_name")
    private String name;
    private String address;

    public Factory() {}

    public Factory(String ico, String name, String address) {
        initialize(ico, name, address);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIco() {
        return ico;
    }

    public Factory setIco(String ico) {
        this.ico = ico;
        return this;
    }

    public String getName() {
        return name;
    }

    public Factory setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Factory setAddress(String address) {
        this.address = address;
        return this;
    }

    private void initialize(String ico, String name, String address) {
        this.ico = ico;
        this.name = name;
        this.address = address;
    }
}
