package cz.kovar.david.xml_import.properties;

import cz.kovar.david.xml_import.models.RegistrationBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public enum Configuration {
    INSTANCE;

    private ClassPathXmlApplicationContext context;
    private RegistrationBean registrationBean;

    public ClassPathXmlApplicationContext getContext() {
        return context;
    }

    public void setContext(ClassPathXmlApplicationContext context) {
        this.context = context;
    }

    public RegistrationBean getRegistrationBean() {
        return registrationBean;
    }

    public void setRegistrationBean(RegistrationBean registrationBean) {
        this.registrationBean = registrationBean;
    }
}
