package cz.kovar.david.xml_import.process;


import cz.kovar.david.xml_import.datasources.DataSourceFactory;
import cz.kovar.david.xml_import.datasources.IDataSource;
import cz.kovar.david.xml_import.models.Employee;
import cz.kovar.david.xml_import.models.Factory;
import cz.kovar.david.xml_import.models.repositories.IEmployeeRepository;
import cz.kovar.david.xml_import.models.repositories.IFactoryRepository;
import cz.kovar.david.xml_import.properties.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;

public class Processor {
    private static final Logger log = LogManager.getLogger();
    private File file;

    private int updates;
    private int inserts;
    private int errors;

    public Processor(String path) {
        file = new File(path);
        if (!file.exists())  {
            log.error("File {} does not exists", path);
            file = null;
            return;
        }

        updates = 0;
        inserts = 0;
        errors = 0;
    }

    public void DoImport() {
        // no import file => nothing to do
        if (file == null) return;

        LocalTime startTime = LocalTime.now();
        log.info("Start import at {}", startTime);

        // initialize source file
        String extension = file.getName().substring(file.getName().lastIndexOf('.')+1).toUpperCase();
        IDataSource ds = DataSourceFactory.INSTANCE.getDataSource(file);
        if (ds == null) {
            log.fatal("Data Source was not initialized. Finishing");
            return;
        }
        log.info("Data Source for type {} created", extension);

        // initialize RowProcessor
        RowProcessor rp = new RowProcessor(extension);
        log.info("Row processor for type {} successfuly initialized", extension);

        try {
            FileInputStream stream = new FileInputStream(file);
            ds.open(stream);

            while (ds.hasNextRow()) {
                HashMap<String, String> row = ds.getNextRow();
                if (row != null) trySave(rp, row);
            }
        } catch (FileNotFoundException e) {
            log.error("File {} was not found", file.getPath());
        }

        // close input stream
        ds.close();

        LocalTime endTime = LocalTime.now();
        Duration duration = Duration.between(startTime, endTime);
        log.info("---------- Import Statisticts --------------");
        log.info("Import finished at {}", endTime);
        log.info("Import took {}", duration.toString());
        log.info("Items created: {} updated: {} in-error: {}", inserts, updates, errors);
    }

    private void trySave(RowProcessor rp, HashMap<String, String> row) {
        if (log.isDebugEnabled())
            log.debug("Save row using row processor {}: {}", rp.getClass().getSimpleName(), String.join("; ", row.values()));

        Employee emp = rp.processNewRow(row, Employee.class);
        IEmployeeRepository employeeRep = Configuration.INSTANCE.getRegistrationBean().getEmployeeRepository();

        if (emp == null) {
            errors++;
        } else {
            Employee oldEmp = employeeRep.findByEmail(emp.getEmail());
            if (oldEmp != null) {
                log.info("Updating employee row");
                updates++;
                oldEmp.setAddress(emp.getAddress())
                      .setFactory(emp.getFactory())
                      .setFirstName(emp.getFirstName())
                      .setLastName(emp.getLastName());
                employeeRep.save(oldEmp);
            } else {
                log.info("Creating employee row");
                inserts++;
                employeeRep.save(emp);
            }

        }

        Factory factory = rp.processNewRow(row, Factory.class);
        IFactoryRepository factoryRep = Configuration.INSTANCE.getRegistrationBean().getFactoryRepository();

        if (factory == null) {
            // in case error was not already counted on employee
            if (emp != null) errors++;
        } else {
            Factory oldFactory = factoryRep.findByIco(factory.getIco());
            if (oldFactory != null) {
                log.info("Updating factory row");
                updates++;
                oldFactory.setAddress(factory.getAddress())
                          .setName(factory.getName());
                factoryRep.save(oldFactory);
            } else {
                log.info("Creating factory row");
                inserts++;
                factoryRep.save(factory);
            }
        }
    }
}
