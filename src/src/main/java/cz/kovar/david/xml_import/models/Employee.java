package cz.kovar.david.xml_import.models;

import cz.kovar.david.xml_import.models.repositories.IFactoryRepository;
import cz.kovar.david.xml_import.properties.Configuration;

import javax.persistence.*;
import java.util.List;

@Entity(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String email;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "first_name")
    private String firstName;
    private String address;
    @OneToOne
    @JoinColumn(name = "factory")
    private Factory factory;

    public Employee() {}

    public Employee(String email, String lastName, String firstName, String address, String factoryName) {
        Factory factory = getFactorybyName(factoryName);
        initialize(email, lastName, firstName, address, factory);
    }

    public Employee(String email, String lastName, String firstName, String address, Factory factory) {
        initialize(email, lastName, firstName, address, factory);
    }

    public void setFactoryByName(String factoryName) {
        factory = getFactorybyName(factoryName);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Employee setAddress(String address) {
        this.address = address;
        return this;
    }

    public Factory getFactory() {
        return factory;
    }

    public Employee setFactory(Factory factory) {
        this.factory = factory;
        return this;
    }

    private void initialize(String email, String lastName, String firstName, String address, Factory factory) {
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.factory = factory;
    }

    private Factory getFactorybyName(String factoryName) {
        IFactoryRepository factoryRep = Configuration.INSTANCE.getRegistrationBean().getFactoryRepository();

        List<Factory> factories = factoryRep.findByName(factoryName);
        if (factories.size() > 0) {
            return factories.get(0);
        } else {
            return null;
        }

    }

}
