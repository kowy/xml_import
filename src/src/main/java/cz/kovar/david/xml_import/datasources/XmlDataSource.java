package cz.kovar.david.xml_import.datasources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

@Component()
public class XmlDataSource implements IDataSource {
    private static final Logger log = LogManager.getLogger();

    private InputStream stream;
    private NodeList rows;
    private int index;

    public XmlDataSource() { }

    public void open(InputStream stream) {
        this.stream = stream;
        // create empty nodeset (in case of any error the rest of the class should work)
        this.rows = new NodeList() {
            @Override
            public Node item(int index) {
                return null;
            }

            @Override
            public int getLength() {
                return 0;
            }
        };
        this.index = 0;

        if (stream == null) {
            return;
        }

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr;
        try {
            expr = xpath.compile("//de");
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            return;
        }

        InputSource is = new InputSource(stream);
        try {
            rows = (NodeList)expr.evaluate(is, XPathConstants.NODESET);
            log.info("Read {} rows from source", rows.getLength());
        } catch (XPathExpressionException e) {
            log.error("Error when evaluating XPath expression: " + e.getMessage(), e);
        }

        log.info("XML stream opening complete");
    }

    public HashMap<String, String> getNextRow() {
        if (index < rows.getLength()) {
            Element row = getNextElement();
            if (row != null) return importRow(row);
        }

        return new HashMap<>();
    }

    public boolean hasNextRow() {
        if (rows == null) return false;

        return (index < rows.getLength()-1);
    }

    public void close() {
        try {
            if (stream != null) stream.close();
        } catch (IOException e) {
            // ignore error (nothing to do)
        }
    }

    private Element getNextElement() {
        Node node = rows.item(index++);
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE)
            return (Element)node;
        else{
            return null;
        }

    }


    private HashMap<String,String> importRow(Element row) {
        NodeList nodes = row.getChildNodes();
        HashMap<String, String> retval = new HashMap<>(nodes.getLength());

        for (int i = 0; i < nodes.getLength(); i++) {
            Node dataNode = nodes.item(i);
            if (dataNode.getNodeType() != Node.ELEMENT_NODE) continue;

            Element dataElm = (Element)dataNode;
            retval.put(dataElm.getTagName(), dataElm.getTextContent());
        }

        return retval;
    }
}
