package cz.kovar.david.xml_import;

import cz.kovar.david.xml_import.models.RegistrationBean;
import cz.kovar.david.xml_import.process.Processor;
import cz.kovar.david.xml_import.properties.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    private static final Logger log = LogManager.getLogger();

    public static void main(String [] args){
        log.info(">>>>>>>>>> Start application >>>>>>>>>>");

        // Acquire Context
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Configuration.INSTANCE.setContext(context);

        // Get RegistrationBean That Defined
        RegistrationBean registrationBean = (RegistrationBean)context.getBean("registrationBean");
        Configuration.INSTANCE.setRegistrationBean(registrationBean);

        try {
            new Processor(args[0]).DoImport();
        } catch (Exception ex) {
            log.error("Unhandled exception: " + ex.getMessage());
            log.debug("Unhandled exception", ex);
        }

        context.close();
        log.info("<<<<<<<<< Application finished <<<<<<<<<<");
    }
}
