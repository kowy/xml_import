package cz.kovar.david.xml_import.models;

import cz.kovar.david.xml_import.models.repositories.IEmployeeRepository;
import cz.kovar.david.xml_import.models.repositories.IFactoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegistrationBean {
    @Autowired
    private IEmployeeRepository employeeRepository;
    @Autowired
    private IFactoryRepository factoryRepository;

    public RegistrationBean() {}

    public IEmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public void setEmployeeRepository(IEmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public IFactoryRepository getFactoryRepository() {
        return factoryRepository;
    }

    public void setFactoryRepository(IFactoryRepository factoryRepository) {
        this.factoryRepository = factoryRepository;
    }
}
