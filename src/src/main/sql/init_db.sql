CREATE DATABASE xml_import
WITH ENCODING='UTF8'
CONNECTION LIMIT=-1;

--DROP TABLE public.employee;
--DROP TABLE public.factory;

-- Table: public.factory

CREATE TABLE public.factory
(
  id SERIAL primary key,
  ico varchar(10) UNIQUE NOT NULL,
  fac_name varchar(255) NOT NULL,
  address character varying(1024)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.factory
OWNER TO postgres;

-- Table: public.employee

CREATE TABLE public.employee
(
  id SERIAL primary key,
  email varchar(255) UNIQUE NOT NULL,
  last_name varchar(255),
  first_name character varying(255),
  address character varying(1024),
  factory integer REFERENCES public.factory (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.employee
OWNER TO postgres;
